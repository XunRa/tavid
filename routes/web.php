<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// Authentication Routes, we only need login and logout
Auth::routes([
    'login'    => true,
    'logout'   => true,
    'register' => false,
    'reset'    => false,
    'confirm'  => false,
    'verify'   => false,
]);

Route::middleware('auth')->group(function () {
    Route::get('/', [
        App\Http\Controllers\Service\ListController::class,
        'index'
    ])->name('home');

    Route::get('/services', [
        App\Http\Controllers\Service\ListController::class,
        'search'
    ])->name('search');

    Route::put('/services', [
        App\Http\Controllers\Service\CreateController::class,
        'store'
    ])->name('store');

    Route::patch('/services/{service}', [
        App\Http\Controllers\Service\CreateController::class,
        'update'
    ])->name('update');
});
