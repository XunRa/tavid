<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRequest;
use App\Models\Service;
use Illuminate\Support\Facades\Auth;

class CreateController extends Controller
{
    /**
     * Updates Service model
     *
     * @param Service      $service
     * @param StoreRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Service $service, StoreRequest $request)
    {
        $service->update($request->validated());

        return response()->noContent();
    }

    /**
     * Creates a new service model, creates a relation with current logged in user.
     *
     * @param StoreRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $service = new Service();
        // Fill new model
        $service->fill($request->validated());
        // Add relation and save
        Auth::user()->services()->save($service);

        return response()->noContent();
    }
}
