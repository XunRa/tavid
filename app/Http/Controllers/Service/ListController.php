<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Http\Requests\ListRequest;
use App\Models\Service;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class ListController extends Controller
{
    /**
     * Returns blade index template with vue code.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('service/list');
    }

    /**
     * Returns search results to vue
     *
     * @param ListRequest $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function search(ListRequest $request)
    {
        // Start building a query, join user
        $query = Service::query()->with('user');

        // Filter logic
        if ($request->exists('filter_column')) {
            if ($request->get('filter_column') === 'user.name') {
                // Since user is a relation it requires a sub-query
                $query = $query->whereHas('user', function (Builder $query) use ($request) {
                    $query->where('name', 'LIKE', '%' . $request->get('filter_value') . '%');
                });
            } else {
                $query = $query->where(
                    $request->get('filter_column'),
                    'LIKE',
                    '%' . $request->get('filter_value') . '%'
                );
            }
        }

        // Date logic
        if ($request->exists('date_before')) {
            $query = $query->whereDate('created_at', '<=', $request->get('date_before'));
        }

        if ($request->exists('date_after')) {
            $query = $query->whereDate('created_at', '>=', $request->get('date_after'));
        }

        // Sorting
        if ($request->exists('sort_column')) {
            if ($request->get('sort_column') !== 'user.name') {
                $query = $query->orderBy(
                    $request->get('sort_column'),
                    $request->get('sort_direction', 'asc')
                );
            } else {
                // Since user is a relation make a sub-query
                $query = $query->orderBy(
                    User::select('name')->whereColumn('services.user_id', 'users.id'),
                    $request->get('sort_direction', 'asc')
                );
            }
        }

        // Paginate and return
        return $query->paginate(10, '*', 'page', $request->get('page', 1));
    }
}
