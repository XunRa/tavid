<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filter_column'  => 'in:name,owner,user.name|required_with:filter_value',
            'filter_value'   => 'required_with:filter_column',
            'date_before'    => 'date|filled',
            'date_after'     => 'date|filled',
            'sort_column'    => 'in:name,owner,user.name,created_at|required_with:sort_direction',
            'sort_direction' => 'in:desc,asc|required_with:sort_column',
            'page'           => 'integer|min:1',
        ];
    }
}
