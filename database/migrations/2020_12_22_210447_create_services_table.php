<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('name', 128)->unique()->comment('Unique service name');
            $table->string('owner', 64)->comment('Owner of the service');
            $table->text('description')->comment('Description of the service');
            $table->boolean('is_active')->default(true)->comment('Is service active');
            $table->bigInteger('user_id')->comment('Who created the service');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
