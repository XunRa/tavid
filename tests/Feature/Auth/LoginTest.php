<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    private $password = 'tavid-demo';

    public function testCanViewALoginForm()
    {
        $response = $this->get($this->loginRoute());

        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
    }

    public function testCannotViewALoginFormWhenAuthenticated()
    {
        $user = User::factory()->make();

        $response = $this->actingAs($user)->get($this->loginRoute());

        $response->assertRedirect($this->guestMiddlewareRoute());
    }

    public function testCanLoginWithCorrectCredentials()
    {
        $user = User::factory()->create([
            'password' => Hash::make($this->password),
        ]);

        $response = $this->post($this->loginRoute(), [
            'email' => $user->email,
            'password' => $this->password,
        ]);

        $response->assertRedirect($this->successfulLoginRoute());
        $this->assertAuthenticatedAs($user);
    }

    public function testRememberMeFunctionality()
    {
        $user = User::factory()->create([
            'id' => rand(1, 100),
            'password' => Hash::make($this->password),
        ]);

        $response = $this->post($this->loginRoute(), [
            'email' => $user->email,
            'password' => $this->password,
            'remember' => 'on',
        ]);

        $user = $user->fresh();

        $response->assertRedirect($this->successfulLoginRoute());
        $response->assertCookie(Auth::guard()->getRecallerName(), vsprintf('%s|%s|%s', [
            $user->id,
            $user->getRememberToken(),
            $user->password,
        ]));
        $this->assertAuthenticatedAs($user);
    }

    public function testCannotLoginWithIncorrectPassword()
    {
        $user = User::factory()->create([
            'password' => Hash::make($this->password),
        ]);

        $response = $this->from($this->loginRoute())->post($this->loginRoute(), [
            'email' => $user->email,
            'password' => 'invalid-password',
        ]);

        $response->assertRedirect($this->loginRoute());
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    public function testCannotLoginWithEmailThatDoesNotExist()
    {
        $response = $this->from($this->loginRoute())->post($this->loginRoute(), [
            'email' => 'nobody@example.com',
            'password' => 'invalid-password',
        ]);

        $response->assertRedirect($this->loginRoute());
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    protected function successfulLoginRoute()
    {
        return route('home');
    }

    protected function loginRoute()
    {
        return route('login');
    }

    protected function guestMiddlewareRoute()
    {
        return route('home');
    }
}
