<?php

namespace Tests\Feature\Service;

use App\Models\Service;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ListTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->be(User::factory()->create());
    }

    public function testCanViewListPage()
    {
        $response = $this->get(route('home'));
        $response->assertStatus(200);
        $response->assertViewIs('service.list');
    }

    public function testWeCanRetrieveServices()
    {
        $services = Service::factory()->count(10)->create()->toArray();

        $this->assertDatabaseCount('services', 10);
        foreach ($services as $service) {
            $this->assertDatabaseHas('services', ['name' => $service['name']]);
        }

        $response = $this->getJson('/services');
        $response->assertStatus(200);
        $this->assertCount(10, $response['data']);
        for ($idx = 0; $idx < count($services); $idx++) {
            $response->assertJsonPath("data.$idx.name", $services[$idx]['name']);
        }
    }

    public function testWeHavePagination()
    {
        Service::factory()->count(100)->create()->toArray();

        $response = $this->getJson('/services');
        $response->assertStatus(200);
        $this->assertCount(10, $response['data']);
        $response->assertJsonPath('current_page', 1);
        $response->assertJsonPath('from', 1);
        $response->assertJsonPath('last_page', 10);
        $response->assertJsonPath('per_page', 10);
        $response->assertJsonPath('to', 10);
        $response->assertJsonPath('total', 100);
    }

    public function testWeCanChangePages()
    {
        Service::factory()->count(100)->create()->toArray();

        $response = $this->getJson('/services?page=2');
        $response->assertStatus(200);
        $this->assertCount(10, $response['data']);
        $response->assertJsonPath('current_page', 2);
        $response->assertJsonPath('from', 11);
        $response->assertJsonPath('last_page', 10);
        $response->assertJsonPath('per_page', 10);
        $response->assertJsonPath('to', 20);
        $response->assertJsonPath('total', 100);
    }

    public function testPagesHaveDifferentServices()
    {
        Service::factory()->count(100)->create()->toArray();

        $response = $this->getJson('/services?page=2');
        $response->assertStatus(200);
        $this->assertCount(10, $response['data']);
        $dataPage2 = $response['data'];

        $response = $this->getJson('/services?page=3');
        $response->assertStatus(200);
        $this->assertCount(10, $response['data']);
        $this->assertNotEquals($response['data'], $dataPage2);
    }

    public function testWeCanSortByName()
    {
        $services = Service::factory()->count(100)->create();

        $response = $this->getJson('/services?sort_column=name&sort_direction=desc');
        $response->assertStatus(200);

        $this->assertEquals($services->sortByDesc('name')->values()->get(0)->name,
            $response['data'][0]['name']);

        $response = $this->getJson('/services?sort_column=name&sort_direction=asc');
        $response->assertStatus(200);

        $this->assertNotEquals($services[0]['name'], $response['data'][0]['name']);
        $this->assertEquals($services->sortBy('name')->values()->get(0)->name,
            $response['data'][0]['name']);
    }

    public function testSortByUserName()
    {
        $services = Service::factory()->count(100)->create();

        $response = $this->getJson('/services?sort_column=user.name&sort_direction=desc');
        $response->assertStatus(200);

        $sorted = $services->sortByDesc(function ($service) {
            return $service->user->name;
        })->values();

        $this->assertEquals($sorted->get(0)->user->name, $response['data'][0]['user']['name']);

        $response = $this->getJson('/services?sort_column=user.name&sort_direction=asc');
        $response->assertStatus(200);

        $this->assertNotEquals($sorted->get(0)->user->name, $response['data'][0]['user']['name']);

        $sorted = $services->sortBy(function ($service) {
            return $service->user->name;
        })->values();
        $this->assertEquals($sorted->get(0)->user->name, $response['data'][0]['user']['name']);
    }

    public function testFilterByName()
    {
        Service::factory()->count(100)->create();
        $service = Service::factory()->create([
            'name' => 'test'
        ]);

        $response = $this->getJson('/services?filter_column=name&filter_value=test');
        $response->assertStatus(200);
        $this->assertCount(1, $response['data']);
        $response->assertJsonPath("data.0.name", $service['name']);
        $response->assertJsonPath("data.0.description", $service['description']);
    }

    public function testFilterByNonExistentName()
    {
        Service::factory()->count(100)->create();

        $response = $this->getJson('/services?filter_column=name&filter_value=testtesttest');
        $response->assertStatus(200);
        $this->assertCount(0, $response['data']);
    }

    public function testFilterByUserName()
    {
        Service::factory()->count(100)->create();
        $service = Service::factory()->create([
            'user_id' => function () {
                return User::factory()->create([
                    'name' => 'Tavid Demo'
                ])->id;
            }
        ]);

        $response = $this->getJson('/services?filter_column=user.name&filter_value=Tavid Demo');
        $response->assertStatus(200);
        $this->assertCount(1, $response['data']);
        $response->assertJsonPath("data.0.name", $service['name']);
        $response->assertJsonPath("data.0.description", $service['description']);
    }

    public function testFilterByNonExistentUserName()
    {
        Service::factory()->count(100)->create();

        $response = $this->getJson('/services?filter_column=user.name&filter_value=Tavid Demo');
        $response->assertStatus(200);
        $this->assertCount(0, $response['data']);
    }

    public function testFilterByDateBefore()
    {
        Service::factory()->count(50)->create([
            'created_at' => strtotime('-3 months')
        ]);
        Service::factory()->count(50)->create([
            'created_at' => strtotime('+3 months')
        ]);

        $response = $this->getJson('/services?date_before=' . date('Y-m-d'));
        $response->assertStatus(200);
        $response->assertJsonPath('total', 50);

        $response = $this->getJson('/services?date_before=' . date('Y-m-d',
                strtotime('+4 months')));
        $response->assertStatus(200);
        $response->assertJsonPath('total', 100);

        $response = $this->getJson('/services?date_before=' . date('Y-m-d',
                strtotime('-4 months')));
        $response->assertStatus(200);
        $response->assertJsonPath('total', 0);
    }

    public function testFilterByDateAfter()
    {
        Service::factory()->count(50)->create([
            'created_at' => strtotime('-3 months')
        ]);
        Service::factory()->count(50)->create([
            'created_at' => strtotime('+3 months')
        ]);

        $response = $this->getJson('/services?date_after=' . date('Y-m-d'));
        $response->assertStatus(200);
        $response->assertJsonPath('total', 50);

        $response = $this->getJson('/services?date_after=' . date('Y-m-d', strtotime('+4 months')));
        $response->assertStatus(200);
        $response->assertJsonPath('total', 0);

        $response = $this->getJson('/services?date_after=' . date('Y-m-d', strtotime('-4 months')));
        $response->assertStatus(200);
        $response->assertJsonPath('total', 100);
    }

    public function testValidationFailsWhenNoSortDirectionWhenSortColumnExists()
    {
        $response = $this->getJson('/services?sort_column=name');
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenNoSortColumnWhenSortDirectionExists()
    {
        $response = $this->getJson('/services?sort_direction=name');
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenNoFilterValueSentButFilterColumnsExists()
    {
        $response = $this->getJson('/services?filter_column=name');
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenNoFilterColumnSentButFilterValueExists()
    {
        $response = $this->getJson('/services?filter_value=name');
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenFilterColumnIsNotAKnownName()
    {
        $response = $this->getJson('/services?filter_column=created_at');
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenPageIsLessThanOne()
    {
        $response = $this->getJson('/services?page=-1');
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenSortColumnIsNotAKnownName()
    {
        $response = $this->getJson('/services?sort_column=updated_at');
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenEmptyOrInvalidDateIsSent()
    {
        $response = $this->getJson('/services?date_before=');
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);

        $response = $this->getJson('/services?date_after=');
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);

        $response = $this->getJson('/services?date_before=555-55-55');
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);

        $response = $this->getJson('/services?date_after=555-55-55');
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }
}
