<?php

namespace Tests\Feature\Service;

use App\Models\Service;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->be(User::factory()->create());
    }

    public function testCanPatchExistingService()
    {
        $service = Service::factory()->create()->get([
            'name',
            'description',
            'id',
            'owner',
            'is_active'
        ])->get(0)->toArray();

        $this->assertDatabaseHas('services', ['name' => $service['name']]);
        $service['description'] = 'test';
        $service['name']        = 'test';
        $service['owner']       = 'test';

        $response = $this->patchJson('/services/' . $service['id'], $service);
        $response->assertStatus(204);

        $this->assertDatabaseHas(
            'services',
            ['name' => 'test', 'description' => 'test', 'owner' => 'test']
        );
    }

    public function testNameIsUnique()
    {
        $original = Service::factory()->create()->get([
            'name',
            'description',
            'id',
            'owner',
            'is_active'
        ])->get(0)->toArray();
        $this->assertDatabaseHas('services', ['name' => $original['name']]);
        $new = Service::factory()->create()->get([
            'name',
            'description',
            'id',
            'owner',
            'is_active'
        ])->get(1)->toArray();
        $this->assertDatabaseHas('services', ['name' => $new['name']]);

        $oldName     = $new['name'];
        $new['name'] = $original['name'];

        $response = $this->patchJson('/services/' . $new['id'], $new);
        $response->assertStatus(500);

        $this->assertDatabaseHas('services', ['name' => $oldName]);
        $this->assertDatabaseHas('services', ['name' => $original['name']]);
    }

    public function testValidationFailsWhenNoNameIsSent()
    {
        $service = Service::factory()->create()->get([
            'description',
            'id',
            'owner',
            'is_active'
        ])->get(0)->toArray();

        $response = $this->patchJson('/services/' . $service['id'], $service);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenNoDescriptionIsSent()
    {
        $service = Service::factory()->create()->get([
            'name',
            'id',
            'owner',
            'is_active'
        ])->get(0)->toArray();

        $response = $this->patchJson('/services/' . $service['id'], $service);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenOwnerIsSent()
    {
        $service = Service::factory()->create()->get([
            'name',
            'description',
            'id',
            'is_active'
        ])->get(0)->toArray();

        $response = $this->patchJson('/services/' . $service['id'], $service);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenNonBooleanIsSent()
    {
        $service              = Service::factory()->create()->get([
            'name',
            'description',
            'id',
            'owner'
        ])->get(0)->toArray();
        $service['is_active'] = 'aaaaaaaaa';

        $response = $this->patchJson('/services/' . $service['id'], $service);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenATooLongNameIsSent()
    {
        $service         = Service::factory()->create()->get([
            'description',
            'id',
            'owner',
            'is_active'
        ])->get(0)->toArray();
        $service['name'] = str_repeat('a', 200);

        $response = $this->patchJson('/services/' . $service['id'], $service);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenATooLongOwnerIsSent()
    {
        $service          = Service::factory()->create()->get([
            'name',
            'description',
            'id',
            'is_active'
        ])->get(0)->toArray();
        $service['owner'] = str_repeat('a', 200);

        $response = $this->patchJson('/services/' . $service['id'], $service);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }
}
