<?php

namespace Tests\Feature\Service;

use App\Models\Service;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->be(User::factory()->create());
    }

    public function testCanCreateANewService()
    {
        $service = Service::factory()->make()->toArray();
        unset($service['user']);

        $this->assertDatabaseMissing('services', ['name' => $service['name']]);

        $response = $this->putJson('/services', $service);
        $response->assertStatus(204);

        $this->assertDatabaseHas('services', ['name' => $service['name']]);
    }

    public function testNameIsUnique()
    {
        $service = Service::factory()->make()->toArray();
        unset($service['user']);

        $this->assertDatabaseMissing('services', ['name' => $service['name']]);

        $response = $this->putJson('/services', $service);
        $response->assertStatus(204);

        $this->assertDatabaseHas('services', ['name' => $service['name']]);

        $response = $this->putJson('/services', $service);
        $response->assertStatus(500);
    }

    public function testValidationFailsWhenNoNameIsSent()
    {
        $service = Service::factory()->make()->toArray();
        unset($service['user']);
        unset($service['name']);

        $response = $this->putJson('/services', $service);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenNoDescriptionIsSent()
    {
        $service = Service::factory()->make()->toArray();
        unset($service['user']);
        unset($service['description']);

        $response = $this->putJson('/services', $service);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenOwnerIsSent()
    {
        $service = Service::factory()->make()->toArray();
        unset($service['user']);
        unset($service['owner']);

        $response = $this->putJson('/services', $service);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenNonBooleanIsSent()
    {
        $service = Service::factory()->make()->toArray();
        unset($service['user']);
        $service['is_active'] = 'aaaaaaaa';

        $response = $this->putJson('/services', $service);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenATooLongNameIsSent()
    {
        $service = Service::factory()->make()->toArray();
        unset($service['user']);
        $service['name'] = str_repeat('a', 200);

        $response = $this->putJson('/services', $service);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }

    public function testValidationFailsWhenATooLongOwnerIsSent()
    {
        $service = Service::factory()->make()->toArray();
        unset($service['user']);
        $service['owner'] = str_repeat('a', 200);

        $response = $this->putJson('/services', $service);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $response);
    }
}
