FROM node:12 AS node
WORKDIR /app
COPY . /app
RUN npm ci
RUN npm run prod

FROM php:8.0-apache AS php
WORKDIR /var/www/html
# Install system dependencies
RUN apt-get update && apt-get install --no-install-recommends -y \
    git \
    wget \
    curl \
    libonig-dev \
    libxml2-dev \
    zip \
    libzip-dev \
    ca-certificates &&\
    apt-get clean &&\
    rm -rf /var/lib/apt/lists/* &&\
    rm -rf /src/*.deb
# PHP local.ini
COPY docker/local.ini /usr/local/etc/php/conf.d/local.ini
# Install PHP extensions
RUN docker-php-ext-install pdo pdo_mysql mbstring zip
# Apache conf
COPY docker/vhost.conf /etc/apache2/sites-available/000-default.conf
# Get latest Composer and install
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && ln -s $(composer config --global home) /root/composer
ENV PATH $PATH:/root/composer/vendor/bin
COPY . /var/www/html
RUN composer install && composer update --no-interaction
COPY --from=node /app/public ./public
# Ready to go
COPY docker/start.sh /usr/local/bin/start
RUN sed -i 's/\x0D$//' /usr/local/bin/start
RUN chmod 0744 /var/www/html/artisan &&\
    chmod u+x /usr/local/bin/start &&\
    chown -R www-data:www-data /var/www/html &&\
    chmod -R 777 /var/www/html/storage &&\
    chmod -R 777 /var/www/html/bootstrap/cache &&\
    a2enmod rewrite
CMD ["/usr/local/bin/start"]
