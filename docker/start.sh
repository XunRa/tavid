#!/usr/bin/env bash

echo "Waiting for mysql to boot up"
sleep 10

echo "Migrating"
(cd /var/www/html && php artisan migrate:fresh --seed --force)

echo "Caching configuration"
(cd /var/www/html && php artisan cache:clear && php artisan optimize && php artisan config:cache && php artisan route:cache && composer dump-autoload)

exec apache2-foreground
