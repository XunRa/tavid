# Tavid developer test

## How to run things

### For testing of the assignment

* Copy/Rename .env.example to .env
``cp .env.example .env``
* Run 
``docker-compose -f docker-compose.prod.yml up --build``
* Open [localhost](http://localhost)
* Email ``demo@demo.ee`` and Password ``demo``

### For development

* Copy/Rename .env.example to .env
* Install php and js packages
``composer install && npm install``
* Run js watcher
``npm run watch``
* Run docker compose for mysql and php
``docker-compose up``
* When changing dockerfile run
``docker-compose up --build``
to rebuild and start

### To run phpunit tests

Clear config from cache to make sure env is set to testing

``php artisan cache:clear && php artisan config:cache``

Then run
``vendor/bin/phpunit`` or ``php artisan test --env=testing``

### To view code coverage, assuming xdebug is installed

``XDEBUG_MODE=coverage vendor/bin/phpunit --coverage-text``

### To run PHP Code sniffer

``vendor/bin/phpcs ./app``

### To run PHP Copy/Paste Detector

``php phpcpd.phar ./app``

